
import wx
import wx.xrc

class AlignViaDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Align Vias parameters", pos = wx.DefaultPosition, size = wx.Size( 200,150 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )

		import sys
		if sys.version_info[0] == 2:
			self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		else:
			self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		fgSizer1 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_alignment = wx.RadioBox(self, wx.ID_ANY, u"Alignment", wx.DefaultPosition, wx.DefaultSize, choices=['Outwards', 'Inwards'], majorDimension=0, style=wx.RA_SPECIFY_ROWS)
		fgSizer1.Add(self.m_alignment, 0, wx.ALL, 5)
		#fgSizer1.Add(self.m_align_out, 0, wx.ALL, 5)
		#fgSizer1.Add(self.m_align_in, 0, wx.ALL, 5)


		bSizer3.Add( fgSizer1, 1, wx.EXPAND, 5 )

		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText101 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText101.Wrap( -1 )

		bSizer1.Add( self.m_staticText101, 1, wx.ALL, 5 )

		self.m_button1 = wx.Button( self, wx.ID_OK, u"Run", wx.DefaultPosition, wx.DefaultSize, 0 )

		self.m_button1.SetDefault()
		bSizer1.Add( self.m_button1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_button2 = wx.Button( self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer1.Add( self.m_button2, 0, wx.ALL, 5 )

		bSizer3.Add( bSizer1, 0, wx.EXPAND|wx.ALIGN_RIGHT, 5 )


		self.SetSizer( bSizer3 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def onDeleteClick( self, event ):
		event.Skip()


