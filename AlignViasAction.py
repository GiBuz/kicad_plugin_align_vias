#
#  AlignViasAction.py
#
#  Copyright 2017 Giulio Borsoi <giulio.borsoi@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import pcbnew as pn
import math
import os
import wx
from . import AlignViaDialog

class AlignViasDialogEx(AlignViaDialog.AlignViaDialog):

    def onDeleteClick(self, event):
        return self.EndModal(wx.ID_DELETE)


def sort_branches(branches, direction):
    locations = []
    for branch in branches:
        locations.append(branch.fixed_tracks[direction]['edge_position'])
    sorted_branches = []
    for position in sorted(locations):
        for branch in branches:
            if branch.fixed_tracks[direction]['edge_position'] == position:
                sorted_branches.append(branch)
    other_direction = 'horizontal'
    if direction == 'horizontal':
        other_direction = 'vertical'
    if branches[0].fixed_tracks[other_direction]['direction'] < 0:
        sorted_branches.reverse()
    return sorted_branches


class AlignViasAction(pn.ActionPlugin):

    def defaults(self):
        self.name = "Via Alignment Tool"
        self.category = "Modify PCB"
        self.description = "Aligns selected vias and packs wires tightly"
        self.icon_file_name = os.path.join(os.path.dirname(__file__), "./align-vias.png")
        self.show_toolbar_button = True

    def Run(self):
        board = pn.GetBoard()
        branches = []  # let's call a via and the 2 segments on each side a "branch"
        for via in board.GetTracks():
            if via.IsSelected() and isinstance(via, pn.VIA):
                branches.append(WireBranch(board, via))
        if len(branches) == 0:
            return
        a = AlignViasDialogEx(None)
        modal_result = a.ShowModal()
        if modal_result == wx.ID_CANCEL:
            return
        if not self.branches_are_consistent(branches):
            raise Exception("Clean up traces around the vias, make sure there are horizontal and vertical traces with the same orientation.")
        branches = sort_branches(branches, 'vertical')
        branches2 = sort_branches(branches, 'horizontal')
        flipped = 1
        if branches2 != branches:
            flipped = -1
        # keep the first via where it is -> can be used to choose where the vias will end up.
        delta_x = 0
        delta_y = 0
        # the spacing is based on via size and clearance - assuming all vias and tracks belong to the same net class
        # the spacing is a multiple of 0.01mm
        delta = math.ceil(((branches[0].via.GetWidth() + 2 * branches[0].via.GetClearance() +
                            branches[0].fixed_tracks['vertical']['track'].GetWidth()) / math.sqrt(2)) * 100) / 100
        if a.m_alignment.GetString(a.m_alignment.GetSelection()) == 'Inwards':
            delta = -delta
            branches.reverse()
        # initialize the previous position to the position of the first branch.
        previous_position = branches[0].via.GetPosition()
        i = 0  # an index to alternate offset between x and y axis
        for branch in branches:  # in these loops, we simply try all possible combinations of endpoints of adjacent tracks and if they overlap we adjust their position
            new_via_position = pn.wxPoint(previous_position.x + delta_x, previous_position.y + delta_y)
            for track in branch.tracks:
                endpoints = {'start': {'get': 'GetStart', 'set': 'SetStart'},
                             'end': {'get': 'GetEnd', 'set': 'SetEnd'}}
                for endpoint_label, endpoint in endpoints.items():
                    if getattr(track, endpoint['get'])() == branch.via.GetPosition():
                        getattr(track, endpoint['set'])(new_via_position)
                    else:
                        for orientation, oriented_track in branch.fixed_tracks.items():
                            for an_endpoint_label, an_endpoint in endpoints.items():
                                if getattr(oriented_track['track'], an_endpoint['get'])() == getattr(track, endpoint['get'])():
                                    sign = branch.fixed_tracks['vertical']['direction'] * branch.fixed_tracks['horizontal']['direction'] * flipped
                                    if orientation == 'horizontal':
                                        new_endpoint_y = getattr(track, endpoint['get'])().y
                                        new_endpoint_x = new_via_position.x + (new_via_position.y - new_endpoint_y) * sign
                                    else:
                                        new_endpoint_x = getattr(track, endpoint['get'])().x
                                        new_endpoint_y = new_via_position.y + (new_via_position.x - new_endpoint_x)  * sign
                                    getattr(track, endpoint['set'])(pn.wxPoint(new_endpoint_x, new_endpoint_y))
                                    getattr(oriented_track['track'], an_endpoint['set'])(pn.wxPoint(new_endpoint_x, new_endpoint_y))
            if (i + 1) % 2:  # first step is in y direction -> TODO: turn it into a parameter in a dialog
                delta_x = 0
                delta_y = delta * branch.fixed_tracks['vertical']['direction'] * flipped
            else:
                delta_x = delta * branch.fixed_tracks['horizontal']['direction']
                delta_y = 0
            i += 1
            branch.via.SetPosition(new_via_position)
            previous_position = new_via_position

    def branches_are_consistent(self, branches):
        vertical_directions = []
        horizontal_directions = []
        for branch in branches:
            vertical_directions.append(branch.fixed_tracks['vertical']['direction'])
            horizontal_directions.append(branch.fixed_tracks['horizontal']['direction'])
        return len(set(vertical_directions)) <= 1 and len(set(horizontal_directions)) <= 1


def get_track_orientation(track):
    orientation = 'horizontal'
    if track.GetEnd().x == track.GetStart().x:
        orientation = 'vertical'
    return orientation


def get_distance(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2)


def remove_track_from_list(track, list_of_tracks):
    wanted_tracks = []
    for a_track in list_of_tracks:
        if a_track.GetEnd() != track.GetEnd() or a_track.GetStart() != track.GetStart():
            wanted_tracks.append(a_track)
    return wanted_tracks


class WireBranch():
    def __init__(self, board, via):
        self.via = via
        self.horizontal_track = None
        self.vertical_track = None
        self.horizontal_edge = None
        self.vertical_edge = None
        self.board = board
        self.tracks = self.get_tracks_at(self.via.GetPosition())
        self.tracks = remove_track_from_list(self.via, self.tracks)
        self.fixed_tracks = {}
        for track in self.tracks:
            fixed_tracks = self.get_next_track(track)
            if len(fixed_tracks) > 2:
                raise Exception("More than two tracks start from this via: not supported!")
            for track in fixed_tracks:
                start, end = self.get_extremities_of_track_from_via(track, self.via)
                orientation = get_track_orientation(track)
                self.fixed_tracks[orientation] = {}
                self.fixed_tracks[orientation]['track'] = track
                if orientation == 'vertical':
                    self.fixed_tracks[orientation]['edge_position'] = track.GetEnd().x
                    self.fixed_tracks[orientation]['direction'] = (end.y - start.y) / abs(track.GetLength())
                else:
                    self.fixed_tracks[orientation]['edge_position'] = track.GetEnd().y
                    self.fixed_tracks[orientation]['direction'] = (end.x - start.x) / abs(track.GetLength())
        if not all([self.fixed_tracks['vertical'], self.fixed_tracks['horizontal']]):
            raise Exception("Could not find tracks with required configuration: horizontal - {} - vertical - {}".format(
                self.horizontal_track, self.vertical_track))
        self.direction = self.get_direction()

    def get_extremities_of_track_from_via(self, track, via):
        end = track.GetEnd()
        start = track.GetStart()
        if get_distance(track.GetEnd(), self.via.GetPosition()) < get_distance(track.GetStart(),
                                                                                         self.via.GetPosition()):
            end = track.GetStart()
            start = track.GetEnd()
        return start, end

    def get_direction(self):
        pass

    def get_clearance(self):
        return self.via.GetClearance() + self.via.GetWidth()

    def get_next_track(self, track):
        position = track.GetEnd()
        if position == self.via.GetPosition():
            position = track.GetStart()
        tracks = self.get_tracks_at(position)
        return remove_track_from_list(track, tracks)

    def get_tracks_at(self, position):
        tracks = []
        for track in self.board.GetTracks():
            if isinstance(track, pn.TRACK):
                if track.GetEnd() == position or track.GetStart() == position:
                    tracks.append(track)
        return tracks


AlignViasAction().register()

