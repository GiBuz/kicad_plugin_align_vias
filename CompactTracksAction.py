#
#  AlignViasAction.py
#
#  Copyright 2017 Giulio Borsoi <giulio.borsoi@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import pcbnew as pn
import math
import os
import wx
from . import CompactTracksDialog


class DegenerateTrackException(Exception):
    pass


ORIENTATIONS = {'x': {'versor': (1, 0), 'orthogonal': 'y', 'name': 'horizontal'},
                'y': {'versor': (0, 1), 'orthogonal': 'x', 'name': 'vertical'},
                '1': {'versor': (1, 1), 'orthogonal': '-1', 'name': 'positive'},
                '-1': {'versor': (1, -1), 'orthogonal': '1', 'name': 'negative'}}

ENDPOINTS = ['Start', 'End']

REVERSED = True

class CompactTracksDialogEx(CompactTracksDialog.CompatcTracksDialog):

    def onDeleteClick(self, event):
        return self.EndModal(wx.ID_DELETE)


def other_endpoint(endpoint):
    if endpoint == ENDPOINTS[0]:
        return ENDPOINTS[1]
    return ENDPOINTS[0]


def sort_branches(branches, direction):
    locations = []
    for branch in branches:
        locations.append(branch.fixed_tracks[direction]['edge_position'])
    sorted_branches = []
    for position in sorted(locations):
        for branch in branches:
            if branch.fixed_tracks[direction]['edge_position'] == position:
                sorted_branches.append(branch)
    other_direction = 'horizontal'
    if direction == 'horizontal':
        other_direction = 'vertical'
    if branches[0].fixed_tracks[other_direction]['direction'] < 0:
        sorted_branches.reverse()
    return sorted_branches


def sort_tracks(tracks, direction):
    locations = []
    for track in tracks:
        locations.append(getattr(track.track.GetEnd(), direction))
    sorted_branches = []
    for position in sorted(locations):
        for track in tracks:
            if getattr(track.track.GetEnd(), direction) == position:
                sorted_branches.append(track)
    return sorted_branches


class CompactTracksAction(pn.ActionPlugin):

    def defaults(self):
        self.name = "Tracks Compacting Tool"
        self.category = "Modify PCB"
        self.description = "Pulls parallel tracks close together"
        self.icon_file_name = os.path.join(os.path.dirname(__file__), "./compact-tracks.png")
        self.show_toolbar_button = True

    def Run(self):
        board = pn.GetBoard()
        tracks = []  # let's call a via and the 2 segments on each side a "branch"
        orientations = []
        for track in board.GetTracks():
            if track.IsSelected() and isinstance(track, pn.TRACK):
                try:
                    a_track = myTrack(track, board)
                    orientation = a_track.get_orientation()
                    orientations.append(orientation)
                    tracks.append(a_track)
                except DegenerateTrackException:
                    pass
        if not len(set(orientations)) <= 1:
            raise Exception("Tracks are not parallel: {}".format(orientations))
        if orientations[0] not in ['x', 'y']:
            raise Exception("Diagonal lines are not supported yet...")
        orientation = orientations[0]
        a = CompactTracksDialogEx(None, ORIENTATIONS[orientation]['name'])
        modal_result = a.ShowModal()
        if modal_result == wx.ID_CANCEL:
            return
        delta = (tracks[0].track.GetWidth() + tracks[0].track.GetClearance())
        orthogonal_orientation = ORIENTATIONS[orientation]['orthogonal']
        tracks = sort_tracks(tracks, orthogonal_orientation)
        reverse_sign = 1
        if a.m_alignment.GetString(a.m_alignment.GetSelection()) in ['Bottom', 'Right']:
            delta = - delta
            tracks.reverse()
            reverse_sign = -1
        last_position = getattr(tracks[0].track.GetEnd(), orthogonal_orientation)
        for track in tracks:
            end = track.track.GetEnd()
            start = track.track.GetStart()
            tracks_at_endpoints = {ENDPOINTS[0]: [], ENDPOINTS[1]: []}
            new_endpoints = {}
            for endpoint in ENDPOINTS:
                this_endpoint = getattr(track.track, 'Get'+endpoint)()
                opposite_endpoint = getattr(track.track, 'Get'+other_endpoint(endpoint))()
                tracks_at_endpoints[endpoint] = track.get_tracks_at(this_endpoint)
                if len(tracks_at_endpoints[endpoint]) == 0:
                    new_endpoint = linear_combination(this_endpoint, opposite_endpoint, 0.9)
                    intermediate_point = linear_combination(new_endpoint, this_endpoint, 0.5)
                    track_width = track.track.GetWidth()
                    track_layer = track.track.GetLayer()
                    track_net = track.track.GetNet()
                    first_new_track = pn.TRACK(board)
                    if orientation == 'y':
                        temporary_end = pn.wxPoint(last_position, intermediate_point.y - math.copysign(1, this_endpoint.y - opposite_endpoint.y) * reverse_sign * (intermediate_point.x - last_position))  # math.copysign(1, this_endpoint.y - opposite_endpoint.y) *
                    elif orientation == 'x':
                        temporary_end = pn.wxPoint(intermediate_point.x - math.copysign(1, this_endpoint.x - opposite_endpoint.x) * reverse_sign * (intermediate_point.y - last_position), last_position)  # math.copysign(1, this_endpoint.x - opposite_endpoint.x) *
                    first_new_track.SetStart(temporary_end)
                    first_new_track.SetEnd(intermediate_point)
                    first_new_track.SetWidth(track_width)
                    first_new_track.SetLayer(track_layer)
                    first_new_track.SetNet(track_net)
                    board.Add(first_new_track)
                    second_new_track = pn.TRACK(board)
                    second_new_track.SetStart(intermediate_point)
                    second_new_track.SetEnd(this_endpoint)
                    second_new_track.SetWidth(track_width)
                    second_new_track.SetLayer(track_layer)
                    second_new_track.SetNet(track_net)
                    board.Add(second_new_track)
                    end = new_endpoint
                    tracks_at_endpoints[endpoint].append({'track': first_new_track, 'endpoint': 'Start'})
            setattr(end, orthogonal_orientation, last_position)
            setattr(start, orthogonal_orientation,  last_position)

            intersection = None
            for endpoint in ENDPOINTS:
                for a_track in tracks_at_endpoints[endpoint]:
                    try:
                        x, y = get_line_intersection(start, end, a_track['track'].GetStart(), a_track['track'].GetEnd())
                        intersection = pn.wxPoint(x, y)
                    except TypeError:
                        intersection = end
                    getattr(a_track['track'], 'Set' + a_track['endpoint'])(intersection)
                new_endpoints[endpoint] = intersection
            for endpoint, position in new_endpoints.items():
                getattr(track.track, "Set"+endpoint)(position)
            last_position += delta


def linear_combination(point1, point2, position):
    newx = point1.x * position + point2.x * (1 - position)
    newy = point1.y * position + point2.y * (1 - position)
    return pn.wxPoint(newx, newy)


def get_line_intersection(p1, p2, p3, p4):
    def line(p1, p2):
        A = (p1.y - p2.y)
        B = (p2.x - p1.x)
        C = (p1.x * p2.y - p2.x * p1.y)
        return A, B, -C

    def intersection(L1, L2):
        D = L1[0] * L2[1] - L1[1] * L2[0]
        Dx = L1[2] * L2[1] - L1[1] * L2[2]
        Dy = L1[0] * L2[2] - L1[2] * L2[0]
        if D != 0:
            x = Dx / D
            y = Dy / D
            return x, y
        else:
            return False

    line1 = line(p1, p2)
    line2 = line(p3, p4)
    return intersection(line1, line2)


def get_track_orientation(track):
    orientation = 'horizontal'
    if track.GetEnd().x == track.GetStart().x:
        orientation = 'vertical'
    return orientation


def get_distance(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2)


def remove_track_from_list(track, list_of_tracks):
    wanted_tracks = []
    for a_track in list_of_tracks:
        if a_track.GetEnd() != track.GetEnd() or a_track.GetStart() != track.GetStart():
            wanted_tracks.append(a_track)
    return wanted_tracks


class myTrack():
    ENDPOINTS = ['Start', 'End']

    def __init__(self, track, board):
        self.track = track
        self.board = board
        if track.GetEnd() == track.GetStart():
            raise DegenerateTrackException("Track start and end coincide: it is a VIA")

    def is_vertical(self):
        return self.track.GetEnd().x == self.track.GetStart().x

    def is_horizontal(self):
        return self.track.GetEnd().y == self.track.GetStart().y

    def get_orientation(self):
        dx = self.track.GetEnd().x - self.track.GetStart().x
        dy = self.track.GetEnd().y - self.track.GetStart().y
        if dx == 0:
            orientation_x = 0
            orientation_y = dy / dy
        else:
            if dy == 0:
                orientation_x = dx / dx
                orientation_y = 0
            else:
                orientation_x = dx / dx
                orientation_y = dy / abs(dy) * math.copysign(1, dx)
        for label, orientation in ORIENTATIONS.items():
            if orientation['versor'] == (orientation_x, orientation_y):
                return label

    def get_adjacent_tracks(self):
        tracks = []
        for endpoint in self.ENDPOINTS:
            some_tracks = self.get_tracks_at(getattr(self.track, 'Get' + endpoint)())
            for a_track in some_tracks:
                if a_track != self.track:
                    tracks.append(a_track)
        return tracks

    def get_tracks_at(self, position):
        tracks = []
        for track in self.board.GetTracks():
            if isinstance(track, pn.TRACK):
                for endpoint in self.ENDPOINTS:
                    if getattr(track, 'Get' + endpoint)() == position:
                        if track.GetEnd() != self.track.GetEnd() or track.GetStart() != self.track.GetStart():
                            tracks.append({'track': track, 'endpoint': endpoint})
        return tracks


class WireBranch():
    def __init__(self, board, via):
        self.via = via
        self.horizontal_track = None
        self.vertical_track = None
        self.horizontal_edge = None
        self.vertical_edge = None
        self.board = board
        self.tracks = self.get_tracks_at(self.via.GetPosition())
        self.tracks = remove_track_from_list(self.via, self.tracks)
        self.fixed_tracks = {}
        for track in self.tracks:
            fixed_tracks = self.get_next_track(track)
            if len(fixed_tracks) > 2:
                raise Exception("More than two tracks start from this via: not supported!")
            for track in fixed_tracks:
                start, end = self.get_extremities_of_track_from_via(track, self.via)
                orientation = get_track_orientation(track)
                self.fixed_tracks[orientation] = {}
                self.fixed_tracks[orientation]['track'] = track
                if orientation == 'vertical':
                    self.fixed_tracks[orientation]['edge_position'] = track.GetEnd().x
                    self.fixed_tracks[orientation]['direction'] = (end.y - start.y) / abs(track.GetLength())
                else:
                    self.fixed_tracks[orientation]['edge_position'] = track.GetEnd().y
                    self.fixed_tracks[orientation]['direction'] = (end.x - start.x) / abs(track.GetLength())
        if not all([self.fixed_tracks['vertical'], self.fixed_tracks['horizontal']]):
            raise Exception("Could not find tracks with required configuration: horizontal - {} - vertical - {}".format(
                self.horizontal_track, self.vertical_track))
        self.direction = self.get_direction()

    def get_extremities_of_track_from_via(self, track, via):
        end = track.GetEnd()
        start = track.GetStart()
        if get_distance(track.GetEnd(), self.via.GetPosition()) < get_distance(track.GetStart(),
                                                                               self.via.GetPosition()):
            end = track.GetStart()
            start = track.GetEnd()
        return start, end

    def get_direction(self):
        pass

    def get_clearance(self):
        return self.via.GetClearance() + self.via.GetWidth()

    def get_next_track(self, track):
        position = track.GetEnd()
        if position == self.via.GetPosition():
            position = track.GetStart()
        tracks = self.get_tracks_at(position)
        return remove_track_from_list(track, tracks)

    def get_tracks_at(self, position):
        tracks = []
        for track in self.board.GetTracks():
            if isinstance(track, pn.TRACK):
                if track.GetEnd() == position or track.GetStart() == position:
                    tracks.append(track)
        return tracks


CompactTracksAction().register()
